// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Epg from './components/Epg/Epg'

Vue.config.productionTip = false

/* eslint-disable no-new */
const vm = new Vue({
  el: '#epg',
  template: '<Epg ref="epg"/>',
  components: { Epg }
})

window.vm = vm
